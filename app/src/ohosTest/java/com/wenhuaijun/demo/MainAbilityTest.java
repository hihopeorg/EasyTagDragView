/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wenhuaijun.demo;

import com.wenhuaijun.easytagdragview.EasyTipDragView;
import com.wenhuaijun.easytagdragview.ResourceTable;
import com.wenhuaijun.easytagdragview.bean.SimpleTitleTip;
import com.wenhuaijun.easytagdragview.bean.Tip;
import com.wenhuaijun.easytagdragview.widget.DragDropViewCore;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Component;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.Image;
import ohos.agp.components.ListContainer;
import ohos.miscservices.timeutility.Time;
import ohos.multimodalinput.event.TouchEvent;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class MainAbilityTest {

    private Ability ability;
    private IAbilityDelegator abilityDelegator;
    private EasyTipDragView easyTipDragView;

    @Before
    public void setUp() throws Exception {
        abilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        ability = EventHelper.startAbility(MainAbility.class);
        EventHelper.waitForActive(ability, 5);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        easyTipDragView = (EasyTipDragView) ability.findComponentById(ResourceTable.Id_easy_tip_drag_view);
    }

    @After
    public void tearDown() throws Exception {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        abilityDelegator.stopAbility(ability);
        abilityDelegator = null;
        EventHelper.clearAbilities();
    }

    @Test
    public void defaultDisplay() {
        Assert.assertTrue(easyTipDragView.isOpen());
        List<Component> childList = easyTipDragView.getDragDropViewCore().getChildList();
        Assert.assertTrue(childList.size() > 0);
    }

    @Test
    public void addData() {
        List<Component> childList = easyTipDragView.getDragDropViewCore().getChildList();
        int size = childList.size();
        ListContainer addGridView = easyTipDragView.getAddGridView();
        DependentLayout child = (DependentLayout) addGridView.getComponentAt(0);
        AbilityDelegatorRegistry.getAbilityDelegator().runOnUIThreadSync(new Runnable() {
            @Override
            public void run() {
                addGridView.executeItemClick(child, 0, 0);
            }
        });
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        List<Component> childListLater = easyTipDragView.getDragDropViewCore().getChildList();
        int size1 = childListLater.size();
        Assert.assertEquals(size + 1, size1);
    }

    @Test
    public void closeDisplay() {
        Image image = (Image) easyTipDragView.findComponentById(ResourceTable.Id_drag_close_img);
        EventHelper.triggerClickEvent(ability, image);
        Assert.assertFalse(easyTipDragView.isOpen());
    }

    @Test
    public void longClickAndDrag() {
        DragDropViewCore dragDropViewCore = easyTipDragView.getDragDropViewCore();
        List<Tip> listData = dragDropViewCore.getListData();
        SimpleTitleTip tip = (SimpleTitleTip) listData.get(0);
        String firstTip = tip.getTip();
        Component child = dragDropViewCore.getChildList().get(0);
        int[] locationOnScreen = child.getLocationOnScreen();
        Component child2 = dragDropViewCore.getChildList().get(1);
        int[] locationOnScreen2 = child2.getLocationOnScreen();
        int distance = locationOnScreen2[0] - locationOnScreen[0];
        simulateLongClickOnScreen(child);
        simulateMoveOnScreen(child, distance);
        List<Tip> listData2 = dragDropViewCore.getListData();
        SimpleTitleTip tip2 = (SimpleTitleTip) listData2.get(1);
        String secondTip = tip2.getTip();
        Assert.assertEquals(firstTip, secondTip);
    }

    @Test
    public void deleteData() {
        DragDropViewCore dragDropViewCore = easyTipDragView.getDragDropViewCore();
        List<Component> childList = dragDropViewCore.getChildList();
        int size = childList.size();
        Component child = dragDropViewCore.getChildList().get(0);
        simulateLongClickOnScreen(child);
        simulateClickOnScreen(child);
        List<Component> childList1 = dragDropViewCore.getChildList();
        int size1 = childList1.size();
        Assert.assertEquals(size - 1, size1);
    }

    private void simulateLongClickOnScreen(Component component) {
        int[] pos = component.getLocationOnScreen();
        pos[0] += component.getWidth() / 2;
        pos[1] += component.getHeight() / 2;

        long downTime = Time.getRealActiveTime();
        long upTime = downTime + 800;
        TouchEvent ev1 = EventHelper.obtainTouchEvent(downTime, downTime, EventHelper.ACTION_DOWN, pos[0], pos[1]);
        abilityDelegator.triggerTouchEvent(ability, ev1);
        try {
            Thread.sleep(800);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        TouchEvent ev2 = EventHelper.obtainTouchEvent(downTime, upTime, EventHelper.ACTION_UP, pos[0], pos[1]);
        abilityDelegator.triggerTouchEvent(ability, ev2);
    }

    private void simulateMoveOnScreen(Component component, int distance) {
        int[] pos = component.getLocationOnScreen();
        pos[0] += component.getWidth() / 2;
        pos[1] += component.getHeight() / 2;

        long downTime = Time.getRealActiveTime();
        long upTime = downTime + 800;
        TouchEvent ev1 = EventHelper.obtainTouchEvent(downTime, downTime, EventHelper.ACTION_DOWN, pos[0], pos[1]);
        abilityDelegator.triggerTouchEvent(ability, ev1);
        try {
            Thread.sleep(800);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        TouchEvent ev2 = EventHelper.obtainTouchEvent(downTime, downTime, EventHelper.ACTION_MOVE, pos[0] + distance, pos[1]);
        abilityDelegator.triggerTouchEvent(ability, ev2);
        TouchEvent ev3 = EventHelper.obtainTouchEvent(downTime, upTime, EventHelper.ACTION_UP, pos[0] + distance, pos[1]);
        abilityDelegator.triggerTouchEvent(ability, ev3);
    }

    private void simulateClickOnScreen(Component component) {
        int[] pos = component.getLocationOnScreen();
        pos[0] += component.getWidth() / 2;
        pos[1] += component.getHeight() / 2;

        long downTime = Time.getRealActiveTime();
        long upTime = downTime;
        TouchEvent ev1 = EventHelper.obtainTouchEvent(downTime, downTime, EventHelper.ACTION_DOWN, pos[0], pos[1]);
        abilityDelegator.triggerTouchEvent(ability, ev1);
        TouchEvent ev2 = EventHelper.obtainTouchEvent(downTime, upTime, EventHelper.ACTION_UP, pos[0], pos[1]);
        abilityDelegator.triggerTouchEvent(ability, ev2);
    }
}