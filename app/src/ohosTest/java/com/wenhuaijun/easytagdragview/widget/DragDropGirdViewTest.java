/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wenhuaijun.easytagdragview.widget;

import com.wenhuaijun.easytagdragview.bean.SimpleTitleTip;
import com.wenhuaijun.easytagdragview.bean.Tip;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class DragDropGirdViewTest {

    private Context context;
    private DragDropGirdView dragDropGirdView;

    @Before
    public void setUp() throws Exception {
        context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        dragDropGirdView = new DragDropGirdView(context);
    }

    @After
    public void tearDown() throws Exception {
        context = null;
        dragDropGirdView = null;
    }

    @Test
    public void addItemData() {
        List<Tip> tipList = new ArrayList<>();
        SimpleTitleTip simpleTitleTip = new SimpleTitleTip();
        simpleTitleTip.setId(123);
        simpleTitleTip.setTip("hello world");
        tipList.add(simpleTitleTip);
        dragDropGirdView.addItemData(tipList);
        DragDropViewCore dragDropViewCore = dragDropGirdView.getDragDropViewCore();
        Assert.assertEquals(1, dragDropViewCore.getChildList().size());
    }

    @Test
    public void addItem() {
        SimpleTitleTip simpleTitleTip = new SimpleTitleTip();
        simpleTitleTip.setId(123);
        simpleTitleTip.setTip("hello world");
        dragDropGirdView.addItem(simpleTitleTip, false);
        List<Tip> results = dragDropGirdView.getResults();
        Assert.assertEquals(1, results.size());
    }

    @Test
    public void removeItem() {
        SimpleTitleTip simpleTitleTip = new SimpleTitleTip();
        simpleTitleTip.setId(123);
        simpleTitleTip.setTip("hello world");
        dragDropGirdView.addItem(simpleTitleTip, false);
        List<Tip> results = dragDropGirdView.getResults();
        Assert.assertEquals(1, results.size());
        dragDropGirdView.removeItem(simpleTitleTip);
        Assert.assertEquals(0, results.size());
    }

    @Test
    public void dp2px() {
        float dp = 100;
        assertEquals(300, dragDropGirdView.dp2px(dp));
    }

    @Test
    public void getDragDropViewCore() {
        DragDropViewCore dragDropViewCore = dragDropGirdView.getDragDropViewCore();
        Assert.assertNotNull(dragDropViewCore);
    }

    @Test
    public void getResults() {
        SimpleTitleTip simpleTitleTip = new SimpleTitleTip();
        simpleTitleTip.setId(123);
        simpleTitleTip.setTip("hello world");
        dragDropGirdView.addItem(simpleTitleTip, false);
        List<Tip> results = dragDropGirdView.getResults();
        Assert.assertEquals(1, results.size());
    }
}