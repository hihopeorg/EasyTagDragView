package com.wenhuaijun.demo;

import com.wenhuaijun.easytagdragview.EasyTipDragView;
import com.wenhuaijun.easytagdragview.ResourceTable;
import com.wenhuaijun.easytagdragview.bean.SimpleTitleTip;
import com.wenhuaijun.easytagdragview.bean.Tip;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.ToastDialog;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.multimodalinput.event.KeyEvent;

import java.util.List;

public class MainAbility extends Ability {
    private EasyTipDragView easyTipDragView;
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        easyTipDragView =(EasyTipDragView)findComponentById(ResourceTable.Id_easy_tip_drag_view);
        //设置已包含的标签数据
        easyTipDragView.setAddData(TipDataModel.getAddTips());
        //设置可以添加的标签数据
        easyTipDragView.setDragData(TipDataModel.getDragTips());
        //在easyTipDragView处于非编辑模式下点击item的回调（编辑模式下点击item作用为删除item）
        easyTipDragView.setSelectedListener(new EasyTipDragView.OnSelectedListener() {
            @Override
            public void onTileSelected(Tip entity, int position, Component view) {
                toast(((SimpleTitleTip) entity).getTip());
            }
        });
        //设置每次数据改变后的回调（例如每次拖拽排序了标签或者增删了标签都会回调）
        easyTipDragView.setDataResultCallback(new EasyTipDragView.OnDataChangeResultCallback() {
            @Override
            public void onDataChangeResult(List<Tip> tips) {
            }
        });
        //设置点击“确定”按钮后最终数据的回调
        easyTipDragView.setOnCompleteCallback(new EasyTipDragView.OnCompleteCallback() {
            @Override
            public void onComplete(List<Tip> tips) {
                toast("最终数据：" + tips.toString());
             //   btn.setVisibility(View.VISIBLE);
            }
        });
        easyTipDragView.open();
    }

    public void toast(String str){
        Component parse = LayoutScatter.getInstance(MainAbility.this).parse(ResourceTable.Layout_ability_text, null, false);
        Text text = (Text) parse.findComponentById(ResourceTable.Id_text);
        text.setText(str);
        new ToastDialog(MainAbility.this).setComponent(parse).setDuration(200).show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode){
            //点击返回键
            case KeyEvent.KEY_BACK:
                //判断easyTipDragView是否已经显示出来
                if(easyTipDragView.isOpen()){
                    if(!easyTipDragView.onKeyBackDown()){
                      //  btn.setVisibility(View.VISIBLE);
                    }
                    return true;
                }
                //....自己的业务逻辑

                break;
        }
        return super.onKeyDown(keyCode, event);
    }
}
