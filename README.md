# EasyTagDragView
**本项目是基于开源项目EasyTagDragView进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/wenhuaijun/EasyTagDragView ）追踪到原项目版本**

#### 项目介绍

- 项目名称：EasyTagDragView
- 所属系列：ohos的第三方组件适配移植
- 功能：标签选择菜单，长按拖动排序，点击增删标签控件
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/wenhuaijun/EasyTagDragView
- 原项目基线版本：无, sha1:b988aeebb461ac5d9543782e62fe3c049c20839b
- 编程语言：Java
- 外部库依赖：无

#### 效果展示

![gif](screenshot/operation.gif)

#### 安装教程

方法1. 

1. 编译library的har包library.har。
2. 启动 DevEco Studio，将编译的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```
4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方法2. 

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```
2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
dependencies {
    implementation 'com.wenhuaijun.ohos:easytagdragview:1.0.1'
}
```

#### 使用说明

##EasyTagDragView的使用

   1.在layout布局里添加控件：

   ```xml
   <com.wenhuaijun.easytagdragview.EasyTipDragView
        ohos:id="$+id:easy_tip_drag_view"
        ohos:height="match_parent"
        ohos:width="match_parent"
        ohos:clickable="true"/>
```

   2.代码中调用

```java
给EasyTagDragView添加标签数据，包括已添加的标签数据和可添加的标签数据。
    添加的数据bean需继承SimpleTitleTip类，可自由增加成员属性。如没特殊需求，可直接使用提供的SimpleTitleTip类。
    注意每个tip的id必须唯一

    //设置已添加的的标签数据
    easyTipDragView.setDragData(TipDataModel.getDragTips());

    //设置可以添加的标签数据
    easyTipDragView.setAddData(TipDataModel.getAddTips());

    //显示EasyTagDragView
    easyTipDragView.open();

    //在easyTipDragView处于非编辑模式下点击item的回调（编辑模式下点击item作用为删除item）
    easyTipDragView.setSelectedListener(new EasyTipDragView.OnSelectedListener() {
        @Override
        public void onTileSelected(Tip entity, int position, Component view) {
            toast(((SimpleTitleTip) entity).getTip());
        }
    });

    //设置每次数据改变后的回调（例如每次拖拽排序了标签或者增删了标签都会回调）
    easyTipDragView.setDataResultCallback(new EasyTipDragView.OnDataChangeResultCallback() {
        @Override
        public void onDataChangeResult(List<Tip> tips) {
            // 处理业务
        }
    });

    //设置点击“确定”按钮后最终数据的回调
    easyTipDragView.setOnCompleteCallback(new EasyTipDragView.OnCompleteCallback() {
        @Override
        public void onComplete(ArrayList<Tip> tips) {
            toast("最终数据：" + tips.toString());
        }
    });

注：当已经显示EasyTagDragView的时候，需监听返回键，
    在编辑模式下点击返回键取消编辑模式。非编辑模式下则关闭EasyTagDragView

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode){
            //点击返回键
            case KeyEvent.KEY_BACK:
                //判断easyTipDragView是否已经显示出来
                if(easyTipDragView.isOpen()){
                    if(!easyTipDragView.onKeyBackDown()){
                        //自己的业务逻辑
                    }
                    return true;
                }
                //....自己的业务逻辑

                break;
        }
        return super.onKeyDown(keyCode, event);
    }

```

#### 版本迭代

- v1.0.1
