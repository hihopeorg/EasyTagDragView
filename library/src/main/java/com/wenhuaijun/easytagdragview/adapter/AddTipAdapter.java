package com.wenhuaijun.easytagdragview.adapter;

import com.wenhuaijun.easytagdragview.ResourceTable;
import com.wenhuaijun.easytagdragview.bean.SimpleTitleTip;
import com.wenhuaijun.easytagdragview.bean.Tip;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.List;

/**
 * Created by Administrator on 2016/5/27 0027.
 */
public class AddTipAdapter extends BaseItemProvider {

    private List<Tip> tips;
    private int column = 4;

    public AddTipAdapter() {
    }

    @Override
    public int getCount() {
        if(tips ==null){
            return 0;
        }
        return tips.size();
    }

    @Override
    public Object getItem(int position) {
        return tips.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        Component parse = LayoutScatter.getInstance(componentContainer.getContext()).parse(ResourceTable.Layout_view_add_item, componentContainer, false);
        Context context = parse.getContext();
        int deviceWidth = context.getResourceManager().getDeviceCapability().width;
        parse.setWidth(AttrHelper.vp2px((deviceWidth  - 8 * (column + 1)) / column, context));
        if (position % column == 0) {
            parse.setMarginLeft(AttrHelper.vp2px(8, context));
            parse.setMarginRight(AttrHelper.vp2px(8, context));
        } else {
            parse.setMarginRight(AttrHelper.vp2px(8, context));
        }
        parse.setMarginBottom(AttrHelper.vp2px(8, context));
        Text text = (Text) parse.findComponentById(ResourceTable.Id_tagview_title);
        String tip = ((SimpleTitleTip)tips.get(position)).getTip();
        text.setText(tip);
        return parse;
    }

    public List<Tip> getData() {
        return tips;
    }

    public void setData(List<Tip> iDragEntities) {
        this.tips = iDragEntities;
    }
    public void refreshData(){
        notifyDataChanged();
    }

    public void setColumn(int count) {
        this.column = count;
    }
}
