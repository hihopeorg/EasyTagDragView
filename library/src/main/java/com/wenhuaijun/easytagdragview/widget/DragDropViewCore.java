/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wenhuaijun.easytagdragview.widget;

import com.wenhuaijun.easytagdragview.bean.Tip;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.ListContainer;
import ohos.agp.utils.Point;
import ohos.app.Context;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DragDropViewCore extends DirectionalLayout implements Component.TouchEventListener, Component.ClickedListener, Component.LongClickedListener {

    private int draggedIndex = -1, lastX = -1, lastY = -1, lastTargetIndex = -1;
    private int xPadding, yPadding;
    private int itemWidth, itemHeight, colCount;
    private ArrayList<Integer> newPositions = new ArrayList<Integer>();
    private static int ANIM_DURATION = 150;
    private ListContainer.ItemClickedListener onItemClickListener;
    private OnRearrangeListener onRearrangeListener;
    private List<Component> childList = new ArrayList<>();
    private ActionListener<TouchEvent> actionUpListener;
    private ActionListener<TouchEvent> actionMoveListener;
    private ItemLongClickListener longClickListener;
    private List<Tip> listData = new ArrayList<>();
    private DragDropListener dragDropListener;

    public DragDropViewCore(Context context) {
        super(context);
        init();
    }

    private void init() {
        super.setClickedListener(this);
        setTouchEventListener(this);
        setLongClickedListener(this);
        setArrangeListener(arrangeListener);
    }

    @Override
    public void addComponent(Component child) {
        super.addComponent(child);
        childList.add(child);
        newPositions.add(-1);
    }

    /**
     * 增加不能进行拖动排序的类 目前的实现该不能拖动的item
     * 只能处于最后一个item
     * add undraggable view to the last child
     *
     * @param child
     */
    public void addUnDraggableView(Component child) {
        child.setTag("undraggable");
        addComponent(child);
    }

    @Override
    public void removeComponentAt(int index) {
        super.removeComponentAt(index);
        newPositions.remove(index);
    }

    ArrangeListener arrangeListener = new ArrangeListener() {
        @Override
        public boolean onArrange(int l, int t, int w, int h) {
            xPadding = (w - (itemWidth * colCount)) / (colCount + 1);
            for (int i = 0; i < getChildCount(); i++) {
                if (i != draggedIndex) {
                    Point xy = getCoorFromIndex(i);
                    getComponentAt(i).arrange((int) xy.getPointX(), (int) xy.getPointY(), itemWidth, itemHeight);
                }

            }
            return true;
        }
    };

    /**
     * get index from coordinate
     *
     * @param x
     * @param y
     * @return
     */
    private int getIndexFromCoor(int x, int y) {
        int col = getColFromCoor(x);
        int row = getRowFromCoor(y);
        if (col == -1 || row == -1) {
            return -1;
        }
        int index = row * colCount + col;
        if (index >= getDraggedChildCount()) {
            return -1;
        }
        return index;
    }

    private int getColFromCoor(int coor) {
        coor -= xPadding;
        for (int i = 0; coor > 0; i++) {
            if (coor < itemWidth) {
                return i;
            }
            coor -= (itemWidth + xPadding);
        }
        return -1;
    }

    private int getRowFromCoor(int coor) {
        coor -= yPadding;
        //不使用group 从低端到顶端进行计算
        for (int i = 0; coor > 0; i++) {
            if (coor < itemHeight) {
                return i;
            }
            coor -= (itemHeight + yPadding);
        }
        return -1;
    }

    /**
     * 判断当前移动到的位置 当当前位置在另一个item区域时交换
     *
     * @param x
     * @param y
     * @return
     */
    private int getTargetFromCoor(int x, int y) {
        if (getRowFromCoor(y) == -1) {
            //touch is between rows
            return -1;
        }
        int target = getIndexFromCoor(x, y);
        //将item移动到最后的item之后
        if (target == getDraggedChildCount()) {
            target = getDraggedChildCount() - 1;
        }
        return target;
    }

    private Point getCoorFromIndex(int index) {
        int col = index % colCount;
        int row = index / colCount;
        return new Point(xPadding + (itemWidth + xPadding) * col,
                yPadding + (itemHeight + yPadding) * row);
    }

    @Override
    public void onClick(Component view) {
        if (onItemClickListener != null && getIndex() != -1) {
            onItemClickListener.onItemClicked(null, getComponentAt(getIndex()), getIndex(), getIndex() / colCount);
        }
    }

    @Override
    public void onLongClicked(Component view) {
        int index = getIndex();
        if (index != -1) {
            //如果长按的位置在
            draggedIndex = index;
            if (longClickListener != null) {
                longClickListener.onItemLongClicked(draggedIndex);

                for (Component component : childList) {
                    DependentLayout child = (DependentLayout) component;
                    child.getComponentAt(1).setVisibility(VISIBLE);
                }
            }
            animateActionDown();
        }
    }

    @Override
    public boolean onTouchEvent(Component view, TouchEvent event) {
        int action = event.getAction();
        int[] location = view.getLocationOnScreen();
        MmiPoint point = event.getPointerScreenPosition(event.getIndex());
        float x = point.getX() - location[0];
        float y = point.getY() - location[1];
        switch (action) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                lastX = (int) x;
                lastY = (int) y;
                break;
            case TouchEvent.POINT_MOVE:
                int deltaX = (int) x - lastX;
                int deltaY = (int) y - lastY;
                if (draggedIndex != -1) {
                    if (actionMoveListener != null) {
                        actionMoveListener.onAction(event);
                    }
                    Component draggedView = getComponentAt(draggedIndex);
                    if (draggedView.getTag() != null && draggedView.getTag().equals("undraggable")) {
                        return false;
                    }
                    int itemLeft = draggedView.getLeft(), itemTop = draggedView.getTop();
                    draggedView.arrange(itemLeft + deltaX, itemTop + deltaY, itemWidth, itemHeight);
                    //得到当前点击位置所在的item的index
                    int targetIndex = getTargetFromCoor((int) x, (int) y);
                    if (lastTargetIndex != targetIndex && targetIndex != -1) {
                        animateGap(targetIndex);
                        lastTargetIndex = targetIndex;
                    }
                }
                lastX = (int) x;
                lastY = (int) y;
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                if (draggedIndex != -1) {
                    if (actionUpListener != null) {
                        actionUpListener.onAction(event);
                    }
                    //如果存在item交换 则重新排列子view
                    if (lastTargetIndex != -1) {
                        reorderChildren();
                    }
                    animateActionUp();
                    lastTargetIndex = -1;
                    draggedIndex = -1;
                }
                break;
        }
        //如果存在拖动item 并且需要消费掉该事件 则返回true
        if (draggedIndex != -1) {
            return true;
        }
        return true;
    }

    /**
     * actionDown动画
     */
    private void animateActionDown() {
        Component v = getComponentAt(draggedIndex);
        AnimatorProperty alpha = new AnimatorProperty(v);
        alpha.alpha(.5f);
        alpha.alphaFrom(1);
        alpha.setDuration(ANIM_DURATION);
        alpha.reset();
        alpha.start();
    }

    /**
     * actionUp动画
     */
    private void animateActionUp() {
        Component v = getComponentAt(draggedIndex);
        AnimatorProperty alpha = new AnimatorProperty(v);
        alpha.alpha(1);
        alpha.alphaFrom(.5f);
        alpha.setDuration(ANIM_DURATION);
        alpha.reset();
        alpha.start();
    }

    /**
     * 拖动某个item时其他item的移动动画 不移动最后不能被拖动的item
     * animate the other item when the dragged item moving
     *
     * @param targetIndex
     */
    private void animateGap(int targetIndex) {
        for (int i = 0; i < getDraggedChildCount(); i++) {
            Component v = getComponentAt(i);
            if (i == draggedIndex) {
                continue;
            }
            int newPos = i;
            if (draggedIndex < targetIndex && i >= draggedIndex + 1 && i <= targetIndex) {
                newPos--;
            } else if (targetIndex < draggedIndex && i >= targetIndex && i < draggedIndex) {
                newPos++;
            }

            //animate
            int oldPos = i;
            if (newPositions.get(i) != -1) {
                oldPos = newPositions.get(i);
            }
            if (oldPos == newPos) {
                continue;
            }

            Point oldXY = getCoorFromIndex(oldPos);
            Point newXY = getCoorFromIndex(newPos);
            AnimatorProperty translate = new AnimatorProperty(v);
            translate.moveFromX(oldXY.getPointX());
            translate.moveToX(newXY.getPointX());
            translate.moveFromY(oldXY.getPointY());
            translate.moveToY(newXY.getPointY());
            translate.reset();
            translate.start();

            newPositions.set(i, newPos);
        }
    }

    private void reorderChildren() {
        //FIGURE OUT HOW TO REORDER CHILDREN WITHOUT REMOVING THEM ALL AND RECONSTRUCTING THE LIST!!!
        ArrayList<Component> children = new ArrayList<>();
        for (int i = 0; i < getChildCount(); i++) {
            children.add(getComponentAt(i));
        }
        removeAllComponents();
        childList.clear();
        int oldIndex = draggedIndex;
        int newIndex = lastTargetIndex;
        while (draggedIndex != lastTargetIndex) {
            if (lastTargetIndex == children.size()) {
                // dragged and dropped to the right of the last element
                children.add(children.remove(draggedIndex));
                listData.add(listData.remove(draggedIndex));
                draggedIndex = lastTargetIndex;
            } else if (draggedIndex < lastTargetIndex) {
                // shift to the right
                Collections.swap(children, draggedIndex, draggedIndex + 1);
                Collections.swap(listData, draggedIndex, draggedIndex + 1);
                draggedIndex++;
            } else if (draggedIndex > lastTargetIndex) {
                // shift to the left
                Collections.swap(children, draggedIndex, draggedIndex - 1);
                Collections.swap(listData, draggedIndex, draggedIndex - 1);
                draggedIndex--;
            }
        }
        for (int i = 0; i < children.size(); i++) {
            newPositions.set(i, -1);
            addComponent(children.get(i));
            Point xy = getCoorFromIndex(i);
            getComponentAt(i).arrange((int) xy.getPointX(), (int) xy.getPointY(), itemWidth, itemHeight);
        }
        notifyDataChange();
        if (onRearrangeListener != null) {
            onRearrangeListener.onRearrange(oldIndex, newIndex);
        }
    }

    /**
     * get the index of dragging item
     *
     * @return
     */
    private int getIndex() {
        return getIndexFromCoor(lastX, lastY);
    }

    public void setOnRearrangeListener(OnRearrangeListener onRearrangeListener) {
        this.onRearrangeListener = onRearrangeListener;
    }

    public void setOnItemClickListener(ListContainer.ItemClickedListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setActionUpListener(ActionListener<TouchEvent> actionUpListener) {
        this.actionUpListener = actionUpListener;
    }

    public void setActionMoveListener(ActionListener<TouchEvent> actionMoveListener) {
        this.actionMoveListener = actionMoveListener;
    }

    public int getDraggedChildCount() {
        int childCount = 0;
        for (int i = 0; i < getChildCount(); i++) {
            if (getComponentAt(i) != null
                    && getComponentAt(i).getTag() != null
                    && getComponentAt(i).getTag().equals("undraggable")) {

                continue;
            }
            childCount++;

        }
        return childCount;
    }

    public int getDraggedIndex() {
        return draggedIndex;
    }

    public List<Component> getChildList() {
        return childList;
    }

    public void setColCount(int colCount) {
        this.colCount = colCount;
    }

    public void setItemHeight(int itemHeight) {
        this.itemHeight = itemHeight;
    }

    public void setyPadding(int yPadding) {
        this.yPadding = yPadding;
    }

    public void setItemWidth(int itemWidth) {
        this.itemWidth = itemWidth;
    }

    public void setData(List<Tip> data) {
        this.listData = data;
    }

    public void addData(Tip tip) {
        listData.add(tip);
    }

    public void removeData(Tip tip) {
        listData.remove(tip);
    }

    public void notifyDataChange() {
        if (dragDropListener != null) {
            dragDropListener.onDataSetChangedForResult(listData);
        }
    }

    // used for junit test
    public List<Tip> getListData() {
        return listData;
    }

    public interface DragDropListener {
        void onDataSetChangedForResult(List<Tip> list);
    }

    public void setDragDropListener(DragDropListener listener) {
        dragDropListener = listener;
    }

    public interface OnRearrangeListener {
        void onRearrange(int oldIndex, int newIndex);
    }

    public interface ActionListener<T> {
        void onAction(T t);
    }

    public interface ItemLongClickListener {
        void onItemLongClicked(int draggedIndex);
    }

    public void setLongClickListener(ItemLongClickListener longClickListener) {
        this.longClickListener = longClickListener;
    }

    public void cancelEditStatus() {
        for (Component component : childList) {
            DependentLayout child = (DependentLayout) component;
            child.getComponentAt(1).setVisibility(HIDE);
        }
    }
}
