/*
 * Copyright 2015 - 2016 solartisan/imilk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wenhuaijun.easytagdragview.widget;

import com.wenhuaijun.easytagdragview.ResourceTable;
import com.wenhuaijun.easytagdragview.bean.SimpleTitleTip;
import com.wenhuaijun.easytagdragview.bean.Tip;
import ohos.agp.components.*;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class DragDropGirdView extends ScrollView {

    public static final String LOG_TAG = DragDropGirdView.class.getSimpleName();
    private DependentLayout rootView;
    private AttrSet attributeSet;
    private int itemCount;
    private int rowPadding;
    private int itemWidth, itemHeight, colCount;
    private int rowCount, groupItemCount;
    private DragDropViewCore dragDropViewCore;

    private static final int DEFAULT_ITEM_HEIGHT = 40;
    private static final int DEFAULT_ITEM_WIDTH = 80;
    private static final int DEFAULT_Y_PADDING = 10;
    private static final int DEFAULT_GROUP_ITEM_COUNT = 8;
    private static final int DEFAULT_GROUP_LINE_COUNT = 2;
    private final List<Tip> results = new ArrayList<>();

    public DragDropGirdView(Context context) {
        this(context, null);
    }

    public DragDropGirdView(Context context, AttrSet attrs) {
        this(context, attrs, "");
    }

    public DragDropGirdView(Context context, AttrSet attrs, String defStyle) {
        super(context, attrs, defStyle);
        attributeSet = attrs;
        init();
    }

    private void init() {
        initView();
        initAttributes();
        initEventListener();
    }

    private void initAttributes() {
        itemHeight = getDimensionValue(attributeSet, "rowHeight", dp2px(DEFAULT_ITEM_HEIGHT));
        itemWidth = getDimensionValue(attributeSet, "itemWidth", dp2px(DEFAULT_ITEM_WIDTH));
        rowPadding = getDimensionValue(attributeSet, "yPadding", dp2px(DEFAULT_Y_PADDING));
        rowCount = getIntegerValue(attributeSet, "rowCount", DEFAULT_GROUP_LINE_COUNT);
        groupItemCount = getIntegerValue(attributeSet, "groupItemCount", DEFAULT_GROUP_ITEM_COUNT);
        colCount = groupItemCount / rowCount;
    }

    private void initView() {
        DirectionalLayout.LayoutConfig scrollViewParam = new DirectionalLayout.LayoutConfig(
                ComponentContainer.LayoutConfig.MATCH_PARENT,
                ComponentContainer.LayoutConfig.MATCH_PARENT);
        setLayoutConfig(scrollViewParam);
        DirectionalLayout linearLayout = new DirectionalLayout(getContext());
        LayoutConfig params = new LayoutConfig(
                LayoutConfig.MATCH_PARENT, LayoutConfig.MATCH_CONTENT);
        linearLayout.setLayoutConfig(params);
        linearLayout.setOrientation(DirectionalLayout.VERTICAL);
        rootView = new DependentLayout(getContext());
        linearLayout.addComponent(rootView);
        super.addComponent(linearLayout);
        dragDropViewCore = new DragDropViewCore(getContext());
    }

    private void initEventListener() {
        //解决和scrollview的上下滚动冲突问题
        dragDropViewCore.setActionMoveListener(new DragDropViewCore.ActionListener<TouchEvent>() {
            @Override
            public void onAction(TouchEvent motionEvent) {
                //requestDisallowInterceptTouchEvent(true);
                int scrollDistance = getScrollValue(Component.AXIS_Y);
                //motionEvent.getY 为其在scrollView中的子view的高度 而不是距离屏幕的高度 getRawY为距离屏幕左上角的高度 注意scrollView中的子view比屏幕的高度要大
                int y = Math.round(motionEvent.getPointerScreenPosition(motionEvent.getIndex()).getY());
                int translatedY = y - scrollDistance;
                int threshold = 50;
                // scrollview 向下移动 将上面的内容显示出来
                if (translatedY < threshold) {
                    scrollBy(0, -30);
                }
                // scrollview 向上移动 将下面的内容显示出来
                if (translatedY + threshold > getHeight()) {
                    // make a scroll down by 30 px
                    scrollBy(0, 30);
                }
            }
        });

        dragDropViewCore.setActionUpListener(new DragDropViewCore.ActionListener<TouchEvent>() {
            @Override
            public void onAction(TouchEvent motionEvent) {
            }
        });
    }

    private void initDraggableView() {
        int rowCount = 0;
        if (itemCount > 0) {
            rowCount = (int) Math.ceil((double) itemCount / colCount);
        }

        DirectionalLayout.LayoutConfig layoutParams = new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_PARENT,
                itemHeight * rowCount + rowPadding * rowCount);
        rootView.removeComponent(dragDropViewCore);

        dragDropViewCore.setLayoutConfig(layoutParams);
        dragDropViewCore.setItemHeight(itemHeight);
        dragDropViewCore.setItemWidth(itemWidth);
        dragDropViewCore.setColCount(colCount);
        dragDropViewCore.setyPadding(rowPadding);
        rootView.addComponent(dragDropViewCore);
    }

    /**
     * should init first
     *
     * @param itemCount
     */
    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
        initDraggableView();
    }

    private void addChildView(Component child) {
        if (dragDropViewCore != null) {
            dragDropViewCore.addComponent(child);
        }
    }

    public void addItemData(List<Tip> data) {
        if (dragDropViewCore == null) return;
        dragDropViewCore.setData(data);
        if (results.size() > 0) {
            results.clear();
        }
        results.addAll(data);
        handleChildResult();
    }

    public void addItem(Tip tip, boolean isEditing) {
        dragDropViewCore.addData(tip);
        results.add(tip);
        DependentLayout component = (DependentLayout) LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_view_tag_item, null, false);
        Text text = (Text) component.findComponentById(ResourceTable.Id_tagview_title);
        Image image = (Image) component.findComponentById(ResourceTable.Id_tagview_delete);
        if (isEditing) {
            image.setVisibility(VISIBLE);
        } else {
            image.setVisibility(HIDE);
        }
        text.setText(((SimpleTitleTip) tip).getTip());
        text.setTextAlignment(TextAlignment.CENTER);
        addChildView(component);
    }

    public void removeItem(Tip tip) {
        results.remove(tip);
        dragDropViewCore.removeData(tip);
    }

    private void handleChildResult() {
        if (dragDropViewCore.getChildCount() > 0) {
            dragDropViewCore.removeAllComponents();
        }
        for (Tip result : results) {
            SimpleTitleTip tip = (SimpleTitleTip) result;
            Component component = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_view_tag_item, null, false);
            Text text = (Text) component.findComponentById(ResourceTable.Id_tagview_title);
            text.setText(tip.getTip());
            text.setTextAlignment(TextAlignment.CENTER);
            addChildView(component);
        }
    }

    public int dp2px(float dp) {
        return AttrHelper.vp2px(dp, getContext());
    }

    private int getIntegerValue(AttrSet attrSet, String name, int defaultValue) {
        if (attrSet == null) {
            return defaultValue;
        }
        try {
            int value = attrSet.getAttr(name).get().getIntegerValue();
            return value;
        } catch (NoSuchElementException e) {
            return defaultValue;
        }
    }

    private int getDimensionValue(AttrSet attrSet, String name, int defaultValue) {
        if (attrSet == null) {
            return defaultValue;
        }
        try {
            int value = attrSet.getAttr(name).get().getDimensionValue();
            return value;
        } catch (NoSuchElementException e) {
            return defaultValue;
        }
    }

    public DragDropViewCore getDragDropViewCore() {
        return dragDropViewCore;
    }

    public List<Tip> getResults() {
        return results;
    }
}
