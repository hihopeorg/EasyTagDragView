package com.wenhuaijun.easytagdragview;

import com.wenhuaijun.easytagdragview.adapter.AddTipAdapter;
import com.wenhuaijun.easytagdragview.bean.SimpleTitleTip;
import com.wenhuaijun.easytagdragview.bean.Tip;
import com.wenhuaijun.easytagdragview.widget.DragDropGirdView;
import com.wenhuaijun.easytagdragview.widget.DragDropViewCore;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wenhuaijun on 2016/5/27 0027.
 */
public class EasyTipDragView extends DependentLayout implements Component.ClickedListener, DragDropViewCore.DragDropListener {
    private DragDropGirdView dragDropGirdView;
    private ListContainer addGridView;
    private Image closeImg;
    private Text completeTv;
    private AddTipAdapter addTipAdapter;
    private boolean isEditing =false;
    private DragDropViewCore dragDropViewCore;
    private OnCompleteCallback completeCallback;
    private OnDataChangeResultCallback dataResultCallback;
    private List<Tip> lists;
    private boolean isOpen= false;
    private OnSelectedListener onSelectedListener;

    public EasyTipDragView(Context context) {
        super(context);
        initView(context);
    }

    public EasyTipDragView(Context context, AttrSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public EasyTipDragView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context){
        close();

        addTipAdapter = new AddTipAdapter();
        //加载view
        Component view = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_view_easytagdrag, this, true);

        dragDropGirdView =(DragDropGirdView)view.findComponentById(ResourceTable.Id_tagdrag_view);
        closeImg =(Image)view.findComponentById(ResourceTable.Id_drag_close_img);
        completeTv =(Text)view.findComponentById(ResourceTable.Id_drag_finish_tv);

        addGridView =(ListContainer) view.findComponentById(ResourceTable.Id_add_gridview);
        TableLayoutManager layoutManager = new TableLayoutManager();
        layoutManager.setColumnCount(4);
        addGridView.setLayoutManager(layoutManager);
        dragDropViewCore = dragDropGirdView.getDragDropViewCore();
        dragDropViewCore.setDragDropListener(this);
        addGridView.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int position, long id) {
                dragDropGirdView.addItem(addTipAdapter.getData().get(position), isEditing);
                dragDropGirdView.setItemCount(dragDropGirdView.getResults().size());
                dragDropViewCore.notifyDataChange();
                addTipAdapter.getData().remove(position);
                addTipAdapter.refreshData();
            }
        });
        dragDropViewCore.setLongClickListener(new DragDropViewCore.ItemLongClickListener() {
            @Override
            public void onItemLongClicked(int draggedIndex) {
                isEditing = true;
                closeImg.setVisibility(HIDE);
                completeTv.setVisibility(VISIBLE);
            }
        });
        dragDropViewCore.setOnItemClickListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int position, long id) {
                Text text = (Text) ((DependentLayout) component).getComponentAt(0);
                String content = text.getText();
                List<Tip> results = dragDropGirdView.getResults();
                Tip clickTip = null;
                for (Tip tip : results) {
                    SimpleTitleTip temp = (SimpleTitleTip)tip;
                    if (temp.getTip().contentEquals(content)) {
                        clickTip = temp;
                        break;
                    }
                }
                if (!isEditing) {
                    if (onSelectedListener != null) {
                        onSelectedListener.onTileSelected(clickTip, position, component);
                    }
                    return;
                }

                dragDropViewCore.getChildList().remove(component);
                dragDropViewCore.removeComponentAt(position);
                dragDropGirdView.removeItem(clickTip);
                dragDropGirdView.setItemCount(dragDropGirdView.getResults().size());
                dragDropViewCore.notifyDataChange();
                addTipAdapter.getData().add(clickTip);
                addTipAdapter.refreshData();
            }
        });
        closeImg.setClickedListener(this);
        completeTv.setClickedListener(this);
    }

    @Override
    public void onDataSetChangedForResult(List<Tip> lists) {
        this.lists = lists;
        if (dataResultCallback != null) {
            dataResultCallback.onDataChangeResult(lists);
        }
    }

    public void setDragData(List<Tip> tips){
        if (tips == null)
            return;
        dragDropGirdView.setItemCount(tips.size());
        dragDropGirdView.addItemData(tips);
    }
    public void setAddData(List<Tip> tips){
        lists = new ArrayList<>(tips);
        addTipAdapter.setData(tips);
        addGridView.setItemProvider(addTipAdapter);
    }

    public void setDataResultCallback(OnDataChangeResultCallback dataResultCallback) {
        this.dataResultCallback = dataResultCallback;
    }

    public void setOnCompleteCallback(OnCompleteCallback callback){
        this.completeCallback =callback;
    }

    public void setSelectedListener(OnSelectedListener selectedListener) {
        onSelectedListener = selectedListener;
    }

    public void close(){
        setVisibility(Component.HIDE);
        isOpen =false;
    }
    public void open(){
        setVisibility(Component.VISIBLE);
        isOpen =true;
    }

    @Override
    public void onClick(Component v) {
        int id = v.getId();
        if (id == ResourceTable.Id_drag_close_img) {
            //关闭，不回调数据
            close();
        } else if (id == ResourceTable.Id_drag_finish_tv) {
            //完成关闭，回调数据
            dragDropViewCore.cancelEditStatus();
            if(completeCallback!=null){
                completeCallback.onComplete(lists);
            }
            close();
        }
    }

    public interface OnDataChangeResultCallback {
        void onDataChangeResult(List<Tip> tips);
    }

    //在最后点击"完成"关闭EasyTipDragView时回调
    public interface OnCompleteCallback{
        void onComplete(List<Tip> tips);
    }

    public interface OnSelectedListener {
        void onTileSelected(Tip entity, int position, Component view);
    }

    public boolean isOpen() {
        return isOpen;
    }
    //点击返回键监听
    public boolean onKeyBackDown(){
        //如果处于编辑模式，则取消编辑模式
        if(isEditing){
            dragDropViewCore.cancelEditStatus();
            isEditing = false;
            return true;
        }else{
            //关闭该view
            close();
            return false;
        }
    }

    public DragDropViewCore getDragDropViewCore() {
        return dragDropViewCore;
    }

    public ListContainer getAddGridView() {
        return addGridView;
    }
}
